SRI Force sensor ros package

This repository contains the source code for the sri force sensor of ros package. This ROS package can be used to build communication between the sri force sensor and ROS. 


For citation please refer to:

Su, H., Yang, C., Ferrigno, G., & De Momi, E. (2019). Improved Human–Robot Collaborative Control of Redundant Robot for Teleoperated Minimally Invasive Surgery. IEEE Robotics and Automation Letters, 4(2), 1447-1453. (This paper is also indexed in IEEE International Conference on Robotics and Automation, 2019.)


Further questions please contact hang.su@polimi.it (www.nearlab.polimi.it) without hesitation. Thanks. 

The package is still under construction. Deailed instructions and Codes with tool identification and force sensor calibration will be uploaded in the following days.

